﻿#pragma once
#include <nlohmann/json.hpp>
#include <time.h>
#include <string>
#include <vector>
#include <chrono>
#include <stdexcept>
#include <utility>
#include <fstream>
#include <functional>
#include <filesystem>
#include <codecvt>

namespace meigetsusoft {
	namespace calendar {
		namespace time_wrapper {
#if defined NO_TIME_S
			namespace {
				inline bool localtime_s(struct tm* result, const time_t* timeval) {
					return !localtime_r(timeval, result);
				}
				inline bool gmtime_s(struct tm* result, const time_t* timeval) {
					return !gmtime_r(timeval, result);
				}
			}
#endif
			tm localtime(const time_t& time) {
				tm timedata{};
				if (localtime_s(&timedata, &time))
					throw std::runtime_error("error in localtime_s function.");
				return timedata;
			}

			tm gmtime(const time_t& time) {
				tm timedata{};
				if (gmtime_s(&timedata, &time))
					throw std::runtime_error("error in gmtime_s function.");
				return timedata;
			}

			tm get_current_clock_l() {
				return localtime(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
			}

			tm get_current_clock_g() {
				return gmtime(std::chrono::system_clock::to_time_t(std::chrono::system_clock::now()));
			}
		}

		namespace japanese_make_impl {
			struct Date {
				Date() = default;
				Date(const int y, const int m, const int d)
					: m_year(y), m_month(m), m_day(d) {}
				Date(const tm& time) : Date(time.tm_year + 1900, time.tm_mon + 1, time.tm_mday) {}
				bool operator == (const tm& timedata) const noexcept {
					return timedata.tm_year + 1900 == this->m_year && timedata.tm_mon + 1 == this->m_month && timedata.tm_mday == this->m_day;
				}
				bool operator != (const Date& timedata) {
					return this->operator==(timedata);
				}
				bool operator < (const tm& timedata) const noexcept {
					return this->m_year < (timedata.tm_year + 1900)
						|| (this->m_year == (timedata.tm_year + 1900) && (this->m_month < timedata.tm_mon + 1))
						|| (this->m_year == (timedata.tm_year + 1900) && (this->m_month == timedata.tm_mon + 1) && this->m_day < timedata.tm_mday);
				}
				bool operator <= (const tm& timedata) const noexcept { return this->operator<(timedata) || this->operator==(timedata); }
				bool operator > (const tm& timedata) const noexcept { return !this->operator<=(timedata); }
				bool operator >= (const tm& timedata) const noexcept { return !this->operator<(timedata); }
				bool operator == (const Date& timedata) const noexcept {
					return timedata.m_year + 1900 == this->m_year && timedata.m_month + 1 == this->m_month && timedata.m_day == this->m_day;
				}
				bool operator != (const Date& timedata) const noexcept {
					return this->operator==(timedata);
				}
				bool operator < (const Date& timedata) const noexcept {
					return this->m_year < (timedata.m_year + 1900)
						|| (this->m_year == (timedata.m_year + 1900) && (this->m_month < timedata.m_month + 1))
						|| (this->m_year == (timedata.m_year + 1900) && (this->m_month == timedata.m_month + 1) && this->m_day < timedata.m_day);
				}
				bool operator <= (const Date& timedata) const noexcept { return this->operator<(timedata) || this->operator==(timedata); }
				bool operator > (const Date& timedata) const noexcept { return !this->operator<=(timedata); }
				bool operator >= (const Date& timedata) const noexcept { return !this->operator<(timedata); }
				int m_year;
				int m_month;
				int m_day;
			};
			struct JapaneseCalendarBorderTable {
				JapaneseCalendarBorderTable() = default;
				JapaneseCalendarBorderTable(const std::string& jcalendar, const std::string& alphabet, const Date& border)
					: m_jcalendar(jcalendar), m_alphabet(alphabet), m_border(border) {}
				// 元号
				std::string m_jcalendar;
				// 元号のアルファベット１文字表記
				std::string m_alphabet;
				// 元号の境目
				Date m_border;
			};
		}

		class japanese_calendar_border_list : public std::vector<japanese_make_impl::JapaneseCalendarBorderTable> {
		private:
			using MyClass = std::vector<japanese_make_impl::JapaneseCalendarBorderTable>;
			std::string get_json_text(const std::string& japanese_calendar_border_json_path) {
				if (!std::filesystem::exists(japanese_calendar_border_json_path)) throw std::runtime_error("japanese calendar file is not found.");
				std::ifstream ifs(japanese_calendar_border_json_path);
				if (ifs.fail()) throw std::runtime_error("Failed to load " + japanese_calendar_border_json_path + ".");
				const std::string json((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
				ifs.close();
				return json;
			}
		public:
			japanese_calendar_border_list(const std::string japanese_calendar_border_json_path = "./japanese_calendar.json") {
				nlohmann::json json = nlohmann::json::parse(this->get_json_text(japanese_calendar_border_json_path));
				if (!json["borders"].is_array()) throw std::runtime_error("json format is invalid");
				for (const auto& i : json["borders"]) {
					if (!i["begin"].is_array()) throw std::runtime_error("json format is invalid");
					std::vector<int> v{};
					for (const auto& j : i["begin"]) v.emplace_back(j.get<int>());
					if (v.size() != 3) throw std::runtime_error("begin date array is wrong.");
					const auto border_data_struct = japanese_make_impl::Date(v.at(0), v.at(1), v.at(2));
					MyClass::emplace_back(
						japanese_make_impl::JapaneseCalendarBorderTable(
							i["jcalendar"].get<std::string>(),
							i["jalphabet"].get<std::string>(),
							border_data_struct
						)
					);
				}
			}
		};

		class japanese {
		private:
			int j_year;
			size_t j_calendar_id;
			std::reference_wrapper<japanese_calendar_border_list> m_japanese_calendar_borders;
			void init_member(const tm& time) {
				for (size_t i = 0; i < this->m_japanese_calendar_borders.get().size(); i++) {
					if (this->m_japanese_calendar_borders.get().at(i).m_border <= time) {
						this->j_calendar_id = i;
						// tm_year + 1900 = 西暦
						// ボーダーの年＝元年(1年)になるので+1をしないと0年となってしまう。このことより和暦を求めるには1901を足す必要がある
						this->j_year = 1901 + time.tm_year - this->m_japanese_calendar_borders.get().at(i).m_border.m_year;
					}
					else break;
				}
			}
			tm timeinfo;
		public:
			japanese(japanese_calendar_border_list& japanese_calendar_borders, const tm& time)
				: timeinfo(time), j_year(1), j_calendar_id(0), m_japanese_calendar_borders(japanese_calendar_borders) {
				if (japanese_calendar_borders.empty()) throw std::runtime_error("japanese calendar border list is empty.");
				this->init_member(time);
			}
			japanese(const japanese&) = delete;
			japanese(japanese&&) = default;
			japanese& operator = (const japanese&) = delete;
			japanese& operator = (japanese&&) = default;
			static japanese localtime(japanese_calendar_border_list& japanese_calendar_borders) { return japanese(japanese_calendar_borders, time_wrapper::get_current_clock_l()); }
			static japanese localtime(japanese_calendar_border_list& japanese_calendar_borders, const time_t& time) { return japanese(japanese_calendar_borders, time_wrapper::localtime(time)); }
			static japanese gmtime(japanese_calendar_border_list& japanese_calendar_borders) { return japanese(japanese_calendar_borders, time_wrapper::get_current_clock_g()); }
			static japanese gmtime(japanese_calendar_border_list& japanese_calendar_borders, const time_t& time) { return japanese(japanese_calendar_borders, time_wrapper::gmtime(time)); }
			operator tm() const noexcept { return this->timeinfo; }
			void update(const tm& new_time) {
				if (japanese_make_impl::Date(new_time) != this->timeinfo) this->init_member(new_time);
				this->timeinfo = new_time;
			}
			// 元号(漢字表記)を取得する
			std::string get_japanese_calendar() const noexcept { return this->m_japanese_calendar_borders.get().at(this->j_calendar_id).m_jcalendar; }
			// 元号(アルファベット１文字表記)を取得する
			std::string get_japanese_a_calendar() const noexcept { return this->m_japanese_calendar_borders.get().at(this->j_calendar_id).m_alphabet; }
			// 和暦年を取得する
			int get_japanese_calendar_year() const noexcept { return this->j_year; }
			// 西暦年を取得する
			int get_anno_domini_year() const noexcept { return this->timeinfo.tm_year + 1900; }
		};
	}
}
