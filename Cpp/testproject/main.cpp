﻿#include "../calendar/japanese.h"
#include <Windows.h>
#include <iostream>

int main() {
	SetConsoleOutputCP(65001);
	try {
		auto time = meigetsusoft::calendar::time_wrapper::get_current_clock_l();
		meigetsusoft::calendar::japanese_calendar_border_list list("../../japanese_calendar.json");
		meigetsusoft::calendar::japanese jap = meigetsusoft::calendar::japanese(list, time);
		const tm timeinfo = static_cast<tm>(jap);
		std::cout << static_cast<const char*>(u8"今日は")
			<< jap.get_japanese_calendar() << jap.get_japanese_calendar_year() << static_cast<const char*>(u8"年")
			<< (timeinfo.tm_mon + 1) << static_cast<const char*>(u8"月")
			<< timeinfo.tm_mday << static_cast<const char*>(u8"日です") << std::endl;
	}
	catch (const std::exception& er) {
		std::cout << er.what() << std::endl;
	}
	return 0;
}
