# 和暦対応カレンダー
和暦に対応したカレンダーです。元号が変わってもJSONファイルに新元号を追記することで即日対応が可能となっております。

本ライブラリは次の言語に対応しています。

- C++
- C#

## C++バージョンのビルド確認済みのコンパイラ
- Microsoft Visual C++ compiler 14.1
- GCC 9.2.0
- Clang 8.0.0

## C#バージョンについて
C#バージョンについては、Microsoft .NET Frameworkプロジェクトでのみビルドおよび動作の確認をしております。

## 依存ライブラリについて
### C++バージョン
- picojson

### C#バージョン
- Newtonsoft Json

## 使い方
### C++バージョン
japanese.hをincludeしてください。クラスの使い方の例はtestprojectのmain.cppを参照して下さい。

また、次の場合にstd::runtime_errorが発生します。

- localtime\_sまたはgmtime\_sでエラーが発生した場合(localtime\_r、gmtime\_rが呼ばれた場合はエラーは発生しません。)
- ボーダーリストを初期化し忘れた場合
- JSONファイルが不完全な場合
- JSONファイルの読み込みに失敗した場合

#### マクロについて
##### CALENDAR\_BUILD\_BEFORE\_CPP14
C++17より古いバージョンでビルドする場合は、このマクロをjapanese.hの前で呼んでください。
##### NO\_TIME\_S
localtime\_s、gmtime\_sではなくlocaltime\_r、gmtime\_rを使う場合は、このマクロをjapanese.hの前で呼んでください。

### C#バージョン
Calendar.csをプロジェクトに組み込んで下さい。クラスの使い方はProgram.csを参照して下さい。

- ボーダーリストの初期化を忘れると、System.InvalidOperationExceptionが投げられます。

## 著作権について
本ライブラリは次の者が著作権を持っております。

**明月ソフト　神御田**

※明月ソフトは、神御田がゲーム等を提供するためのブランドです。

※神御田とあいめぐは同一人物です
