﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;
using System.Globalization;

namespace MeigetsuSoft
{
    class JsonLoadInformation
    {
        [JsonProperty("jcalendar")]
        public string JapaneseCalendar { get; set; }
        [JsonProperty("jalphabet")]
        public string JapaneseCalendarAlphabet { get; set; }
        [JsonProperty("begin")]
        public List<int> BorderList { get; set; }

    }
    class JsonLoadInfoList
    {
        [JsonProperty("borders")]
        public List<JsonLoadInformation> list;
    }
    class DateBorderInfo
    {
        public DateBorderInfo(string jcal, string jalpha, int y, int m, int d)
        {
            JCalendar = jcal;
            JAlphabet = jalpha;
            Year = y;
            Month = m;
            Day = d;
        }
        public static DateBorderInfo ConvertFromJsonLoadInformation(JsonLoadInformation json)
        {
            if (json.BorderList.Count != 3) throw new IOException("begin info is wrong");
            return new DateBorderInfo(json.JapaneseCalendar, json.JapaneseCalendarAlphabet, json.BorderList[0], json.BorderList[1], json.BorderList[2]);
        }
        public string JCalendar;
        public string JAlphabet;
        public int Year;
        public int Month;
        public int Day;
    }
    class JapaneseCalendar
    {
        private void InitJapaneseCalendarData(int y, int m, int d)
        {
            for (int i = 0; i < DateBorderInfoList.Count; i++)
            {
                DateBorderInfo info = DateBorderInfoList[i];
                if (
                    info.Year < y
                    || (info.Year == y && info.Month < m)
                    || (info.Year == y && info.Month == m && info.Day <= d)
                    )
                {
                    JCalendarID = i;
                    JYear = y - info.Year + 1;
                }
                else break;
            }
        }
        public JapaneseCalendar(int y, int m, int d)
        {
            if (DateBorderInfoList.Count == 0)
                throw new System.InvalidOperationException("border list has not been initialized yet.");
            Year = y;
            Month = m;
            Day = d;
            InitJapaneseCalendarData(y, m, d);
        }
        public JapaneseCalendar(DateTime date)
        {
            if (DateBorderInfoList.Count == 0)
                throw new System.InvalidOperationException("border list has not been initialized yet.");
            Calendar cal = CultureInfo.InvariantCulture.Calendar;
            Year = cal.GetYear(date);
            Month = cal.GetMonth(date);
            Day = cal.GetDayOfMonth(date);
            InitJapaneseCalendarData(cal.GetYear(date), cal.GetMonth(date), cal.GetDayOfMonth(date));
        }
        public static void LoadBorderList(string JapaneseCalendarJsonPath = "./japanese_calendar.json", string FileEncode = "UTF-8")
        {
            DateBorderInfoList = new List<DateBorderInfo>();
            string json = File.ReadAllText(JapaneseCalendarJsonPath, Encoding.GetEncoding(FileEncode));
            List<JsonLoadInformation> data = JsonConvert.DeserializeObject<JsonLoadInfoList>(json).list;
            foreach(JsonLoadInformation i in data)
            {
                DateBorderInfoList.Add(DateBorderInfo.ConvertFromJsonLoadInformation(i));
            }
        }
        public string GetJapaneseCalendar()
        {
            return DateBorderInfoList[JCalendarID].JCalendar;
        }
        public string GetJapaneseCalendarAlphabet()
        {
            return DateBorderInfoList[JCalendarID].JAlphabet;
        }
        public int GetJapaneseCalendarYear()
        {
            return JYear;
        }
        public int GetYear()
        {
            return Year;
        }
        public int GetMonth()
        {
            return Month;
        }
        public int GetDayOfMonth()
        {
            return Day;
        }
        private int Year;
        private int Month;
        private int Day;
        private int JYear;
        private int JCalendarID;
        private static List<DateBorderInfo> DateBorderInfoList;
    }
}
