﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MeigetsuSoft
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                JapaneseCalendar.LoadBorderList(@"E:\Users\AinoMegumi\Bitbucket\Calendar\japanese_calendar.json");
                JapaneseCalendar cal = new JapaneseCalendar(DateTime.Now);
                Console.WriteLine("今日は" + cal.GetJapaneseCalendar() + cal.GetJapaneseCalendarYear().ToString() + "年" + cal.GetMonth().ToString() + "月" + cal.GetDayOfMonth().ToString() + "日です。");
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }
    }
}
