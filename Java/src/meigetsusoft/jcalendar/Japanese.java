package meigetsusoft.jcalendar;
import java.io.IOException;
import java.util.Calendar;

public class Japanese {
	private static JapaneseCalendarBorderList list;
	public static void LoadBorderList(String JapaneseCalendarJsonPath) throws IOException {
		list = new JapaneseCalendarBorderList(JapaneseCalendarJsonPath);
	}
	public static void LoadBorderList() throws IOException {
		LoadBorderList("./japanese_calendar.json");
	}
	private int Year, Month, Day, JYear, JCalendarID;
	public Japanese() {
		this(Calendar.getInstance());
	}
	public Japanese(Calendar cal) {
		this(
			cal.get(Calendar.YEAR),
			cal.get(Calendar.MONTH) + 1,
			cal.get(Calendar.DAY_OF_MONTH)
		);
	}
	public Japanese(int y, int m, int d) {
		this.Year = y;
		this.Month = m;
		this.Day = d;
		for (int i = 0; i < list.size(); i++) {
			JapaneseCalendarInfo info = list.get(i);
			if (
				info.YearBorder < y
				|| (info.YearBorder == y && info.MonthBorder < m)
				|| (info.YearBorder == y && info.MonthBorder == m && info.DayBorder <= d)
				) {
				this.JCalendarID = i;
				this.JYear = y - info.YearBorder + 1;
			}
			else break;
		}
	}
	public int GetJapaneseYear() {
		return this.JYear;
	}
	public int GetYear() {
		return this.Year;
	}
	public int GetMonth() {
		return this.Month;
	}
	public int GetDayOfMonth() {
		return this.Day;
	}
	public String GetJapaneseCalendar(){
		return list.get(this.JCalendarID).JapaneseCalendar;
	}
	public String GetJapaneseCalendarAlphabet() {
		return list.get(this.JYear).JapaneseCalendarAlphabet;
	}
}
