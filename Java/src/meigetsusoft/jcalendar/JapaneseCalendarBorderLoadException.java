package meigetsusoft.jcalendar;

import java.io.IOException;

public class JapaneseCalendarBorderLoadException extends IOException {
	public JapaneseCalendarBorderLoadException() {
		super();
	}
	public JapaneseCalendarBorderLoadException(String Message) {
		super(Message);
	}
}
