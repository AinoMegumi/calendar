package meigetsusoft.jcalendar;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JapaneseCalendarBorderList extends ArrayList<JapaneseCalendarInfo> {
	private ArrayList<Integer> GetBorderList(JsonNode ArrayData) throws JapaneseCalendarBorderLoadException {
		if (ArrayData.size() != 3) throw new JapaneseCalendarBorderLoadException("border data is wrong.");
		ArrayList<Integer> RetVal = new ArrayList<Integer>();
		for (int i = 0; i < ArrayData.size(); i++) RetVal.add(ArrayData.get(i).asInt());
		return RetVal;
	}
	public JapaneseCalendarBorderList(String JapaneseCalendarJsonPath) throws IOException {
		ObjectMapper mapper = new ObjectMapper();
		JsonNode root = mapper.readTree(new File(JapaneseCalendarJsonPath));
		for (JsonNode n : root.get("borders")) this.add(new JapaneseCalendarInfo(	n.get("jcalendar").asText(), n.get("jalphabet").asText(), GetBorderList(n.get("begin"))));
	}
}
