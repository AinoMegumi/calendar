package meigetsusoft.jcalendar;
import java.util.ArrayList;

public class JapaneseCalendarInfo {
	private static final int YearBorderPos = 0;
	private static final int MonthBorderPos = 1;
	private static final int DayBorderPos = 2;
	public JapaneseCalendarInfo(String jcalendar, String jcalendarAlphabet, ArrayList<Integer> Borders) {
		this.JapaneseCalendar = jcalendar;
		this.JapaneseCalendarAlphabet = jcalendarAlphabet;
		this.YearBorder = Borders.get(YearBorderPos);
		this.MonthBorder = Borders.get(MonthBorderPos);
		this.DayBorder = Borders.get(DayBorderPos);
	}
	public String JapaneseCalendar;
	public String JapaneseCalendarAlphabet;
	public int YearBorder;
	public int MonthBorder;
	public int DayBorder;
}
